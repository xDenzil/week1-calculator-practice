import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTest {
	
	@Test
	public void shouldAddTwoNumbers() {
		Calculator calc = new Calculator();
		float result = calc.add(10, 10);
		assertEquals(20,result,0);
	}

}