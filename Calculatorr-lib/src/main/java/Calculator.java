/**
 * 
 * @author denzilwilliams
 *
 */
public class Calculator {
	/**
	 * Adds <strong>3</strong> numbers.
	 * @param num1 First number
	 * @param num2 Second number
	 * @return Addition of both numbers
	 * **/
	public float add(int num1, int num2) {
		return num1+num2;
	}

}
